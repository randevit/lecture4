package sbp.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayImplTest {

    /**
     * create CustomArrayImpl
     * check  .size() is equal 0
     * add item with value 34
     * check  .size() is equal 1
     * check  .contains(34) is true
     * add item with value 136
     * check  .size() is equal 2
     * check  .contains(136) is true
     * check  .contains(34) is true
     */
    @Test
    public void testAddIncrement() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        Assertions.assertEquals(0, customArray.size());
        customArray.add(34);
        Assertions.assertEquals(1, customArray.size());
        Assertions.assertTrue(customArray.contains(34));
        customArray.add(136);
        Assertions.assertEquals(2, customArray.size());
        Assertions.assertTrue(customArray.contains(136));
        Assertions.assertTrue(customArray.contains(34));
    }

    /**
     * create arrayAdd with values "234","home","21.14ewrt"
     * create customArray
     * check .size() is 0
     * check .isEmpty() is true
     * add arrayAdd
     * check .size() is 3
     * check .isEmpty() is false
     * check values .get(0), .get(1), .get(2)
     * check .contains() for "234","home","21.14ewrt" are true
     */
    @Test
    public void testAddAllWithAddArrayOfString() {
        String[] arrayAdd = new String[]{"234","home","21.14ewrt"};
        CustomArray customArray = new CustomArrayImpl<String>();
        Assertions.assertEquals(0, customArray.size());
        Assertions.assertTrue(customArray.isEmpty());
        customArray.addAll(arrayAdd);
        Assertions.assertEquals(3, customArray.size());
        Assertions.assertFalse(customArray.isEmpty());
        Assertions.assertEquals("234", customArray.get(0));
        Assertions.assertEquals("home", customArray.get(1));
        Assertions.assertEquals("21.14ewrt", customArray.get(2));
        Assertions.assertTrue(customArray.contains("234"));
        Assertions.assertTrue(customArray.contains("home"));
        Assertions.assertTrue(customArray.contains("21.14ewrt"));
    }

    /**
     * create customArray
     * check throw IllegalArgumentException with .addAll((Integer[])null)
     */
    @Test
    public void testExceptionAddAllWithAddNullArray() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        Assertions.assertThrows(IllegalArgumentException.class,()->customArray.addAll((Integer[]) null));
    }

    /**
     * create list with values 23,123453,9153,0
     * create customArray
     * check .size() is 0
     * check .isEmpty() is true
     * add list
     * check .size() is 4
     * check .isEmpty() is false
     * check values .get(0), .get(1), .get(2), .get(3)
     * check .contains() for "23,123453,9153,0 are true
     */
    @Test
    public void testAddAllWithAddCollectionOfInteger() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(23);
        list.add(123453);
        list.add(9153);
        list.add(0);
        CustomArray customArray = new CustomArrayImpl<Integer>();
        Assertions.assertEquals(0, customArray.size());
        Assertions.assertTrue(customArray.isEmpty());
        customArray.addAll(list);
        Assertions.assertEquals(4, customArray.size());
        Assertions.assertFalse(customArray.isEmpty());
        Assertions.assertEquals(23, customArray.get(0));
        Assertions.assertEquals(123453, customArray.get(1));
        Assertions.assertEquals(9153, customArray.get(2));
        Assertions.assertEquals(0, customArray.get(3));
        Assertions.assertTrue(customArray.contains(23));
        Assertions.assertTrue(customArray.contains(123453));
        Assertions.assertTrue(customArray.contains(9153));
        Assertions.assertTrue(customArray.contains(0));
    }
    /**
     * create customArray
     * check throw IllegalArgumentException with .addAll((List<Integer>)null)
     */
    @Test
    public void testExceptionAddAllWithAddNullCollection() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        Assertions.assertThrows(IllegalArgumentException.class,()->customArray.addAll((List<Integer>) null));
    }

    /**
     * create arrayAdd (Integer[] -> {3,5,19,-10,8767})
     * create customArray
     * add values 100, -100, -1000
     * add arrayAdd to customArray
     * check customArray.size()
     * check values .get(1), .get(3), .get(4), .get(5), .get(6), .get(7)
     */
    @Test
    public void testAddAllWithAddArrayOfIntegerFromIndex() {
        Integer[] arrayAdd = new Integer[]{3,5,19,-10,8767};
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.add(100);
        customArray.add(-100);
        customArray.add(-1000);
        customArray.addAll(1,arrayAdd);
        Assertions.assertEquals(8, customArray.size());
        Assertions.assertEquals(3,customArray.get(1));
        Assertions.assertEquals(19,customArray.get(3));
        Assertions.assertEquals(-10,customArray.get(4));
        Assertions.assertEquals(8767,customArray.get(5));
        Assertions.assertEquals(-100,customArray.get(6));
        Assertions.assertEquals(-1000,customArray.get(7));
    }
    /**
     * create customArray
     * check throw IllegalArgumentException with .addAll(2,(Integer[])null)
     */
    @Test
    public void testAddAllWithAddArrayisNull() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.add(100);
        customArray.add(-100);
        customArray.add(-1000);
        Assertions.assertThrows(IllegalArgumentException.class,()->customArray.addAll(2,(Integer[])null));
    }

    /**
     * create customArray
     * check throw ArrayIndexOutOfBoundsException with .addAll(-1,Integer[]{123})
     * check throw ArrayIndexOutOfBoundsException with .addAll(100,Integer[]{123})
     */
    @Test
    public void testAddAllWithAddArrayWithIndexOutOfBounds() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.add(100);
        customArray.add(-100);
        customArray.add(-1000);
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.addAll(-1,new Integer[]{1,2,3}));
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.addAll(100,new Integer[]{1,2,3}));

    }

    /**
     * create customArray
     * add values "", "13", "home"
     * check .get(0), .get(1), .get(2)
     */
    @Test
    public void testGet() {
        CustomArray customArray = new CustomArrayImpl<String>();
        customArray.add("");
        customArray.add("13");
        customArray.add("home");
        Assertions.assertEquals("", customArray.get(0));
        Assertions.assertEquals("13", customArray.get(1));
        Assertions.assertEquals("home", customArray.get(2));
    }

    /**
     * create customArray
     * add values "", "13", "home", "home2"
     * check .get(3)
     * check ArrayIndexOutOfBoundsException for .get(-100), .get(100)
     */
    @Test
    public void testGetForException() {
        CustomArray customArray = new CustomArrayImpl<String>();
        customArray.add("");
        customArray.add("13");
        customArray.add("home");
        customArray.add("home2");

        Assertions.assertEquals("home2", customArray.get(3));
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.get(-100));
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.get(100));
    }

    /**
     * create customArray
     * add values "", "13", "home"
     * change values in indexes 0,2
     * check .get(0), .get(2)
     */
    @Test
    public void testSet() {
        CustomArray customArray = new CustomArrayImpl<String>();
        customArray.add("");
        customArray.add("13");
        customArray.add("home");
        customArray.set(0,"123");
        customArray.set(2,"12");
        Assertions.assertEquals("123", customArray.get(0));
        Assertions.assertEquals("12", customArray.get(2));
    }

    /**
     * create customArray
     * add values "", "13", "home"
     * check ArrayIndexOutOfBoundsException for .set(-11,"222")
     * check ArrayIndexOutOfBoundsException for .set(10,"")
     */
    @Test
    public void testSetForException() {
        CustomArray customArray = new CustomArrayImpl<String>();
        customArray.add("");
        customArray.add("13");
        customArray.add("home");

        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.set(-11,"222"));
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.set(10,""));
    }

    /**
     * create customArray
     * add values
     * check .size()
     * .remove(2)
     * check .size()
     * check .get(2), .get(3)
     * .remove(3)
     * check .get(3)
     */
    @Test
    public void testRemoveByIndex() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45,-67,15,98698});
        Assertions.assertEquals(7,customArray.size());
        customArray.remove(2);
        Assertions.assertEquals(6,customArray.size());
        Assertions.assertEquals(45, customArray.get(2));
        Assertions.assertEquals(-67, customArray.get(3));
        customArray.remove(3);
        Assertions.assertEquals(15, customArray.get(3));
    }

    /**
     * create customArray
     * add values
     * check ArrayIndexOutOfBoundsException for .remove(-11)
     * check ArrayIndexOutOfBoundsException for .remove(10)
     */
    @Test
    public void testRemoveByIndexForException() {
        List<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(18);
        CustomArray customArray = new CustomArrayImpl<Integer>(list);

        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.remove(-11));
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class,()->customArray.remove(10));
    }

    /**
     * create customArray
     * add values
     * check .size()
     * .remove((Integer)34))
     * check .size()
     * check .get(2), .get(3)
     * .remove((Integer)(-67)))
     * check .get(3)
     */
    @Test
    public void testRemoveByItem() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45,-67,15,98698});
        Assertions.assertEquals(7,customArray.size());
        customArray.remove((Integer)(34));
        Assertions.assertEquals(6,customArray.size());
        Assertions.assertEquals(45, customArray.get(2));
        Assertions.assertEquals(-67, customArray.get(3));
        customArray.remove((Integer)(-67));
        Assertions.assertEquals(15, customArray.get(3));
    }


    /**
     * create customArray
     * add values {1,-15,34,45,-67,15,98698}
     * check .contains() for values -15, 98698
     */
    @Test
    public void testContainsItem() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45,-67,15,98698});
        Assertions.assertTrue(customArray.contains(-15));
        Assertions.assertTrue(customArray.contains(98698));
    }

    /**
     * create customArray
     * add values {1,-15,34,45,-67,15,98698}
     * check .indexOf() for values -15, 98698
     */
    @Test
    public void testIndexOf() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45,-67,15,98698});
        Assertions.assertEquals(1,customArray.indexOf(-15));
        Assertions.assertEquals(6, customArray.indexOf(98698));
    }


    /**
     * create customArray
     * add values {1,-15,34,45,-67,15,98698}
     * check (.size()+incrementCapacity <= .getCapacity()) for  incrementCapacity equal 100 and 1000
     */
    @Test
    public void testEnsureCapacity() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45,-67,15,98698});
        int incrementCapacity = 100;
        customArray.ensureCapacity(incrementCapacity);
        Assertions.assertTrue((customArray.size()+incrementCapacity) <= customArray.getCapacity());
        incrementCapacity = 1000;
        customArray.ensureCapacity(incrementCapacity);
        Assertions.assertTrue((customArray.size()+incrementCapacity) <= customArray.getCapacity());
    }

    /**
     * create customArray with values {1,-15,34,45,-67,15,98698}
     * check IllegalArgumentException for .ensureCapacity(-10)
     * check IllegalArgumentException for .ensureCapacity(-1000)
     */
    @Test
    public void testEnsureCapacityExceptionForIllegalCountOfEnsureElement() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45,-67,15,98698});

        Assertions.assertThrows(IllegalArgumentException.class,()->customArray.ensureCapacity(-10));
        Assertions.assertThrows(IllegalArgumentException.class,()->customArray.ensureCapacity(-1000));
    }

    /**
     * create customArray
     * add values {1,-15,34,45,-67}
     * .reverse()
     * check reverse values
     */
    @Test
    public void testReverse() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45,-67});
        customArray.reverse();
        Assertions.assertEquals(-67, customArray.get(0));
        Assertions.assertEquals(45, customArray.get(1));
        Assertions.assertEquals(34, customArray.get(2));
        Assertions.assertEquals(-15, customArray.get(3));
        Assertions.assertEquals(1, customArray.get(4));
    }

    /**
     * create customArray
     * add values {1,-15,34,45}
     * create newArray by .toArray()
     * check size of newArray
     * check values in newArray
     */
    @Test
    public void testToArray() {
        CustomArray customArray = new CustomArrayImpl<Integer>();
        customArray.addAll(new Integer[]{1,-15,34,45});
        Object[] newArray = customArray.toArray();
        Assertions.assertEquals(customArray.size(), newArray.length);
        Assertions.assertEquals(1, (Integer) newArray[0]);
        Assertions.assertEquals(-15, (Integer) newArray[1]);
        Assertions.assertEquals(34, (Integer) newArray[2]);
        Assertions.assertEquals(45, (Integer) newArray[3]);
    }

}
