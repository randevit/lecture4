package sbp.comparators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * create list as ArrayList
 * create CustomDigitComparator
 * create trueListAfterSort with true values
 * sort list with CustomDigitComparator
 * check by .equals list and trueListAfterSort
 */
public class CustomDigitComparatorTest {
    @Test
    void customDigitComparatorTest() {
        List<Integer> list = new ArrayList<>();
        for (int i = 6; i > 0;i--) {
            list.add(i);
        }

        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        list.sort(customDigitComparator);
        List<Integer> trueListAfterSort = new ArrayList<>();
        trueListAfterSort.add(2);
        trueListAfterSort.add(4);
        trueListAfterSort.add(6);
        trueListAfterSort.add(1);
        trueListAfterSort.add(3);
        trueListAfterSort.add(5);
        Assertions.assertTrue((trueListAfterSort.equals(list)));

    }
}
