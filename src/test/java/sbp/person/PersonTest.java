package sbp.person;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * create 5 Person
 * person1 and person5 equals by their fields another not
 * checking for equality of all combinations
 */
public class PersonTest {
    @Test
    public void testEqualsForPerson() {
        Person pers1 = new Person("Вася","Москва",22);
        Person pers2 = new Person("Коля","Москва",22);
        Person pers3 = new Person("Вася","Москва",23);
        Person pers4 = new Person("Коля","Омск",22);
        Person pers5 = new Person("Вася","Москва",22);
        Assertions.assertTrue(pers1.equals(pers5));
        Assertions.assertFalse(pers1.equals(pers2));
        Assertions.assertFalse(pers2.equals(pers4));
        Assertions.assertFalse(pers3.equals(pers1));
        Assertions.assertFalse(pers4.equals(pers2));
        Assertions.assertFalse(pers4.equals(pers5));
    }

    /**
     * create ArrayList with 5 elements of Person
     * sort it
     * create ArrayList with expert order
     * comparing them for equality
     */
    @Test
    public void testSortOfArrayPerson() {
        List<Person> list = new ArrayList<>();
        list.add(new Person("Вася","Москва",22));
        list.add(new Person("Федор","Москва",32));
        list.add(new Person("Ольга","Омск",18));
        list.add(new Person("Елена","Иркутск",65));
        list.add(new Person("Вася","Омск",45));
        Collections.sort(list);
        List<Person> trueListAfterSort = new ArrayList<>();
        trueListAfterSort.add(new Person("Елена","Иркутск",65));
        trueListAfterSort.add(new Person("Вася","Москва",22));
        trueListAfterSort.add(new Person("Федор","Москва",32));
        trueListAfterSort.add(new Person("Вася","Омск",45));
        trueListAfterSort.add(new Person("Ольга","Омск",18));
        Assertions.assertTrue(list.equals(trueListAfterSort));
    }
}
