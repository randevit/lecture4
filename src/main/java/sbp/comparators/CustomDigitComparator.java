package sbp.comparators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Comparator<Integer> Implementation with order first even then odd
 */
public class CustomDigitComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer lhs, Integer rhs) {
        boolean evenOfLhs = (lhs%2 == 0);
        boolean evenOfRhs = (rhs%2 == 0);
        if (evenOfLhs == evenOfRhs) {
            return (lhs - rhs);
        }
        if (evenOfLhs) {
            return -1;
        }
        if (!evenOfLhs) {
            return 1;
        }
        return 0;
    }

}
