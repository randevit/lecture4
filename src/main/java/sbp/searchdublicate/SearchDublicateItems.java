package sbp.searchdublicate;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Поиск дубликатов в списках с произвольными типами объектов T
 * Дубликаты ищутся во входящем списке
 * Дубликаты возвращаются в отсортированном списке, чтобы проще было тестировать
 * @param <T>
 */
public class SearchDublicateItems<T> {
    /**
     * Используется поиск дубликатов в предварительно отсортированном списке
     * В отсортированном списке дубликаты находятся рядом
     * При проходе списка проверяются соседние элементы
     * @param inputList
     * @return
     */
    public List<T> searchDublicateItems1(List<T> inputList) {
        inputList = inputList.stream().sorted().collect(Collectors.toList());
        Set<T> setOfDublicate = new HashSet<>();
        for (int i = 0; i < (inputList.size() - 1);i++) {
            if (inputList.get(i).equals(inputList.get(i+1))) {
                setOfDublicate.add(inputList.get(i));
            }
        }
        return setOfDublicate.stream().sorted().collect(Collectors.toList());
    }

    /**
     * Используется лобовой поиск дубликатов с добавлением элементов из коллекции в множество
     * с проверкой вхождения: если такй элемент уже есть в множестве, то это дубликат
     * @param inputList
     * @return
     */
    public List<T> searchDublicateItems2(List<T> inputList) {
        Set<T> setUniItems = new HashSet<>();
        List<T> listDublicateItems = new ArrayList<>();
        for (T item: inputList) {
            if (setUniItems.add(item)) {
                listDublicateItems.add(item);
            }
        }
        return listDublicateItems.stream().sorted().collect(Collectors.toList());
    }

    /**
     * Используется превращение входящего списка во множество,
     * затем происходит обход по множеству с помощью итератора
     * Затем у стрима от входящего списка с помощью метода .filter()
     * создается исходящий список с элементами равными элементу из множества
     * и если размер этого списка больше единицы, то этот элемент имеет дубликат
     * @param inputList
     * @return
     */
    public List<T> searchDublicateItems3(List<T> inputList) {
        List<T> listDublicateItems = new ArrayList<>();
        Set<T> setOfList = inputList.stream().collect(Collectors.toSet());
        Iterator<T> setIterator = setOfList.iterator();
        while(setIterator.hasNext()) {
            T itemOfSet = setIterator.next();
            if(inputList.stream().filter(x -> x.equals(itemOfSet)).collect(Collectors.toList()).size() > 1) {
                listDublicateItems.add(itemOfSet);
            }
        }

        return listDublicateItems.stream().sorted().collect(Collectors.toList());
    }


}

