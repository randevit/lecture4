package sbp.exceptions;

public class WorkWithExceptionClient {
	public static void main(String[] args) {
		WorkWithExceptions workWithExceptions = new WorkWithExceptions();
		try {
			workWithExceptions.exceptionProcessing();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
