package sbp.db;

import sbp.person.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Data Access Object
 * Объект доступа к базе данных
 */
public class PersonDAO {
    private DBService db;

    public PersonDAO(String URL) {
        this.db = new DBService(URL);
    }

    /**
     * Добавление одной записи
     * @param person
     * @return
     */
    public boolean addPerson(Person person) {
        String createPersonQuery = "insert into person (name,city,age) values ('%s','%s','%s')";
        return db.executeUpdate(String.format(createPersonQuery,person.getName(),person.getCity(),person.getAge())) > 0;
    }

    /**
     * Просмотр всех записей
     * @return
     */
    public List<Person> readAllPersons() {
        String selectAllQuery = "select * from person";
        List<Person> list = new ArrayList<>();
        ResultSet resultSet = db.executeQuery(selectAllQuery);
        try {
            while (resultSet.next()) {
                Person localPerson = new Person(resultSet.getString("name"),
                        resultSet.getString("city"),
                        resultSet.getInt("age"));
                list.add(localPerson);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }


        return list;
    }

    /**
     * Удаление одной записи с id
     * @param id
     * @return
     */
    public int deletePersonById(int id) {
        String deleteSQL = "delete from person where id = '%s'";
        return db.executeUpdate(String.format(deleteSQL,id));
    }

}
