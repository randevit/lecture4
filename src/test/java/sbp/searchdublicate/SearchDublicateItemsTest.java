package sbp.searchdublicate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class SearchDublicateItemsTest {

    /**
     * check searchDublicateItems1 for List<Integer>
     */
    @Test
    public void searchDublicateItems1TestForInteger() {
        SearchDublicateItems<Integer> searchDublicateItems = new SearchDublicateItems<>();

        Assertions.assertTrue(Arrays.asList(3,7,8).equals(searchDublicateItems.searchDublicateItems1(Arrays.asList(3,6,7,5,8,15,7,3,9,23,7,8))));
    }

    /**
     * check searchDublicateItems2 for List<Integer>
     */
    @Test
    public void searchDublicateItems2TestForInteger() {
        SearchDublicateItems<Integer> searchDublicateItems = new SearchDublicateItems<>();
        Assertions.assertTrue(Arrays.asList(3,7,8).equals(searchDublicateItems.searchDublicateItems2(Arrays.asList(3,6,7,5,8,15,7,3,9,23,7,8))));
    }

    /**
     * check searchDublicateItems3 for List<Integer>
     */
    @Test
    public void searchDublicateItems3TestForInteger() {
        SearchDublicateItems<Integer> searchDublicateItems = new SearchDublicateItems<>();
        Assertions.assertTrue(Arrays.asList(3,7,8).equals(searchDublicateItems.searchDublicateItems3(Arrays.asList(3,6,7,5,8,15,7,3,9,23,7,8))));
    }

    /**
     * check searchDublicateItems1 for List<String>
     */
    @Test
    public void searchDublicateItems1TestForString() {
        SearchDublicateItems<String> searchDublicateItems = new SearchDublicateItems<>();

        Assertions.assertTrue(Arrays.asList("3be","7ii").equals(
                searchDublicateItems.searchDublicateItems1(Arrays.asList("3be","qwr6","7ii","5","8","15","7ii","3be"))));
    }

    /**
     * check searchDublicateItems2 for List<String>
     */
    @Test
    public void searchDublicateItems2TestForString() {
        SearchDublicateItems<String> searchDublicateItems = new SearchDublicateItems<>();
        Assertions.assertTrue(Arrays.asList("3be","7ii").equals(
                searchDublicateItems.searchDublicateItems2(Arrays.asList("3be","qwr6","7ii","5","8","15","7ii","3be"))));
    }

    /**
     * check searchDublicateItems3 for List<String>
     */
    @Test
    public void searchDublicateItems3TestForString() {
        SearchDublicateItems<String> searchDublicateItems = new SearchDublicateItems<>();
        Assertions.assertTrue(Arrays.asList("3be","7ii").equals(
                searchDublicateItems.searchDublicateItems3(Arrays.asList("3be","qwr6","7ii","5","8","15","7ii","3be"))));
    }
}
