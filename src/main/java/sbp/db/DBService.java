package sbp.db;

import java.sql.*;

public class DBService {
    private String URL;

    public DBService(String URL) {
        this.URL = URL;
    }

    /**
     * Запрос на получение данных
     * @param sql
     * @return
     */
    public ResultSet executeQuery(String sql) {
        try (Connection connection = DriverManager.getConnection(this.URL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql);
            ) {
            return resultSet;
        } catch (SQLException ex) {
            return null;
        }
    }

    /**
     * Запрос на изменение данных
     * @param sql
     * @return
     */
    public int executeUpdate(String sql) {
        try (Connection connection = DriverManager.getConnection(this.URL);
             Statement statement = connection.createStatement();
        ) {
            int affectedRows = statement.executeUpdate(sql);
            return affectedRows;
        } catch (SQLException ex) {
            return 0;
        }
    }

}
