package sbp.io;

public class MyIOExampleClient {
	public static void main(String[] args) {
		MyIOExample myIOExample = new MyIOExample();
		System.out.println(myIOExample.workWithFile("MyInFile.txt"));
		System.out.println(myIOExample.copyFile("MyInFile.txt","MyOutFile.txt"));
		System.out.println(myIOExample.workWithFile("MyOutFile.txt"));
		System.out.println(myIOExample.copyBufferedFile("MyInFile.txt","MyOutFile.txt"));
		System.out.println(myIOExample.workWithFile("MyOutFile.txt"));
		System.out.println(myIOExample.copyFileWithReaderAndWriter("MyInFile.txt","MyOutFile.txt"));
		System.out.println(myIOExample.workWithFile("MyOutFile.txt"));
	}
}
