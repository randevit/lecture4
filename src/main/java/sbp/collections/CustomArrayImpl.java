package sbp.collections;

import java.util.Collection;

/**
 * .arrayOfObjects is array with values
 * @param <T>
 */
public class CustomArrayImpl<T> implements CustomArray<T>{
    private final static int SIZE_ARRAY_INCREMENT_CAPACITY = 2;

    private Object[] arrayOfObjects;
    private int sizeOfArray;
    private int countOfArrayPageCapacity;


    public  CustomArrayImpl() {
        arrayOfObjects = new Object[SIZE_ARRAY_INCREMENT_CAPACITY];
        sizeOfArray = 0;
        countOfArrayPageCapacity = 1;
    }

    public CustomArrayImpl(int capacity) {
        countOfArrayPageCapacity = (int)Math.ceil((double)capacity / SIZE_ARRAY_INCREMENT_CAPACITY);
        arrayOfObjects = new Object[SIZE_ARRAY_INCREMENT_CAPACITY*countOfArrayPageCapacity];
        sizeOfArray = 0;
    }

    public CustomArrayImpl(Collection<T> c) {
        this();
        addAll(c);
    }

    /**
     * if index is out of capacity - expand capacity per SIZE_ARRAY_INCREMENT_CAPACITY items
     * @param newArrayLength - last index
     * @return - false if there is exception, else true
     */
    private boolean expandArrayCapacity(int newArrayLength) {
        int newCountOfArrayPageCapacity = (int)Math.ceil((double)(newArrayLength) / SIZE_ARRAY_INCREMENT_CAPACITY);
        if (newCountOfArrayPageCapacity > countOfArrayPageCapacity) {
            try {
                Object[] newArrayOfObjects = new Object[SIZE_ARRAY_INCREMENT_CAPACITY * newCountOfArrayPageCapacity];
                for (int i = 0; i < sizeOfArray; i++) {
                    newArrayOfObjects[i] = arrayOfObjects[i];
                }
                arrayOfObjects = newArrayOfObjects;
                countOfArrayPageCapacity = newCountOfArrayPageCapacity;
            } catch (Exception ex) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int size() {
        return sizeOfArray;
    }

    @Override
    public boolean isEmpty() {
        return (sizeOfArray == 0);
    }

    /**
     * Add single item.
     */
    @Override
    public boolean add(T item) {
        if (expandArrayCapacity(sizeOfArray + 1)) {
            sizeOfArray++;
            arrayOfObjects[sizeOfArray - 1] = item;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add all items.
     *
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter 'items' is null");
        }
        if (expandArrayCapacity(sizeOfArray + items.length)) {
            for (T item: items) {
                sizeOfArray++;
                arrayOfObjects[sizeOfArray - 1] = item;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add all items.
     *
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(Collection<T> items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter 'items' is null");
        }
        if (expandArrayCapacity(sizeOfArray + items.size())) {
            for (T item: items) {
                sizeOfArray++;
                arrayOfObjects[sizeOfArray - 1] = item;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add items to current place in array.
     *
     * @param index - index
     * @param items - items for insert
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     * @throws IllegalArgumentException       if parameter items is null
     */
    @Override
    public boolean addAll(int index, T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter 'items' is null");
        }
        if (index < 0 || index >= sizeOfArray) {
            throw new ArrayIndexOutOfBoundsException("index is out of bounds ("+index+")");
        }
        if (expandArrayCapacity(sizeOfArray + items.length)) {
            for (int i = sizeOfArray - 1; i >= index; i--) {
                arrayOfObjects[items.length  + i] = arrayOfObjects[i];
                arrayOfObjects[index + i] = items[i];
            }
            for (int i = 0; i < items.length; i++) {
                arrayOfObjects[index + i] = items[i];
            }
            sizeOfArray+=items.length;
            return true;
        } else {
            return false;
        }
    }


    /**
     * Get item by index.
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T get(int index) {
        if (index < 0 || index >= sizeOfArray) {
            throw new ArrayIndexOutOfBoundsException("index is out of bounds ("+index+")");
        }
        return (T)arrayOfObjects[index];
    }

    /**
     * Set item by index.
     *
     * @param index - index
     * @return old value
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T set(int index, T item) {
        if (index < 0 || index >= sizeOfArray) {
            throw new ArrayIndexOutOfBoundsException("index is out of bounds ("+index+")");
        }
        T oldItem = (T)arrayOfObjects[index];
        arrayOfObjects[index] = item;
        return oldItem;
    }

    /**
     * Remove item by index.
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public void remove(int index) {
        if (index < 0 || index >= sizeOfArray) {
            throw new ArrayIndexOutOfBoundsException("index is out of bounds ("+index+")");
        }
        for (int i = index; i < (sizeOfArray - 1); i++) {
            arrayOfObjects[i] = arrayOfObjects[i+1];
        }
        sizeOfArray--;
    }

    /**
     * Remove item by value. Remove first item occurrence.
     *
     * @param item - item
     * @return true if item was removed
     */
    @Override
    public boolean remove(T item) {
        int indexRemoveItem = -1;
        for (int i =0;i < sizeOfArray;i++) {
            if (item.equals(arrayOfObjects[i])) {
                indexRemoveItem = i;
                break;
            }
        }
        if (indexRemoveItem > -1) {
            remove(indexRemoveItem);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if item exists.
     *
     * @param item - item
     * @return true or false
     */
    @Override
    public boolean contains(T item) {
        for (Object it :arrayOfObjects) {
            if (item.equals(it)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Index of item.
     *
     * @param item - item
     * @return index of element or -1 of list doesn't contain element
     */
    @Override
    public int indexOf(T item) {
        int indexRemoveItem = -1;
        for (int i =0;i < sizeOfArray;i++) {
            if (item.equals(arrayOfObjects[i])) {
                indexRemoveItem = i;
                break;
            }
        }
        return indexRemoveItem;
    }

    /**
     * Grow current capacity to store new elements if needed.
     *
     * @param countOfEnsureElements - new elements count
     */
    @Override
    public void ensureCapacity(int countOfEnsureElements) {
        if (countOfEnsureElements < 0) {
            throw new IllegalArgumentException("Parameter 'newElementsCount' less than 0");
        }
        expandArrayCapacity(sizeOfArray+countOfEnsureElements);
    }

    /**
     * Get current capacity.
     */
    @Override
    public int getCapacity() {
        return SIZE_ARRAY_INCREMENT_CAPACITY*countOfArrayPageCapacity;
    }

    /**
     * Reverse list.
     */
    @Override
    public void reverse() {
        Object[] newArrayOfObjects = new Object[SIZE_ARRAY_INCREMENT_CAPACITY * countOfArrayPageCapacity];
        for (int i = 0;i < sizeOfArray;i++) {
            newArrayOfObjects[sizeOfArray - 1 - i] = arrayOfObjects[i];
        }
        arrayOfObjects=newArrayOfObjects;
    }

    @Override
    public String toString() {
        String outString = "'[";
        for (int i = 0; i < sizeOfArray;i++) {
            outString+=" "+(T)arrayOfObjects[i];
        }
        outString+=" ]'";
        return outString;
    }

    /**
     * Get copy of current array.
     */
    @Override
    public Object[] toArray() {
        Object[] outArray = new Object[sizeOfArray];
        for(int i =0;i<sizeOfArray;i++) {
            outArray[i]=arrayOfObjects[i];
        }
        return outArray;
    }

    public static void main(String[] args) {
        CustomArray customArray = new CustomArrayImpl<Double>();
        customArray.addAll(new Double[]{4.34,6765.8,-12.0,1.0/3});
        System.out.println(customArray);
    }
}
