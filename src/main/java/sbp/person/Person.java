package sbp.person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * class Person with fields -> name, city, age
 * and with implementation Comparable<Person>
 * with sort by @city at first and @name at last
 */
public class Person implements Comparable<Person>{
    private String name;
    private String city;
    private int age;

    public Person(String name, String city, int age) {
        if (name == null) {
            throw new IllegalArgumentException("Parameter @name is null");
        } else {
            this.name = name;
        }
        if (city == null) {
            throw new IllegalArgumentException("Parameter @city is null");
        } else {
            this.city = city;
        }
        this.age = age;
    }

    @Override
    public int compareTo(Person other) {
        if (this.city.compareTo(other.city)==0) {
            return this.name.compareTo(other.name);
        } else {
            return this.city.compareTo(other.city);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && name.equals(person.name) && city.equals(person.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, city, age);
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age='" + age + '\'' +
                '}';
    }



}
