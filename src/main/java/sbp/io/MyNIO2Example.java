package sbp.io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MyNIO2Example
{
    /**
     * Создать объект класса {@link Path}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link Files}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName)
    {
        Path path = Paths.get(fileName);
        boolean isRealPath = Files.exists(path);
        if (isRealPath) {
            System.out.println(path.toAbsolutePath());
            System.out.println(path.getParent());
            if (Files.isRegularFile(path)) {
                try {
                    System.out.println(Files.size(path));
                    System.out.println(Files.getLastModifiedTime(path));
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать NIO2 классы {@link  java.nio.file.Path} и {@link Files}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        try {
            Files.copy(Paths.get(sourceFileName),Paths.get(destinationFileName));
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать NIO2 классы {@link  java.io.BufferedReader} и {@link  java.io.BufferedWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        Path pathInput = Paths.get(sourceFileName);
        Path pathOut = Paths.get(destinationFileName);
        boolean isSuccessfulCopy = false;
        try (BufferedReader bufferedReader = Files.newBufferedReader(pathInput);
             BufferedWriter bufferedWriter = Files.newBufferedWriter(pathOut)
            ) {
                char[] bufferChar = new char[1024];
                int sizeReadBlock;
                do {
                    sizeReadBlock = bufferedReader.read(bufferChar, 0, bufferChar.length);
                    if (sizeReadBlock > 0) {
                        bufferedWriter.write(bufferChar, 0, sizeReadBlock);
                    }
                } while (sizeReadBlock > 0);
                bufferedWriter.flush();
                isSuccessfulCopy = true;

            } catch (IOException eio) {
                System.out.println("Проблемы с вводом/выводом");
                eio.printStackTrace();

            } catch (Exception ex) {
                ex.printStackTrace();
        }

        return isSuccessfulCopy;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать NIO2 классы {@link java.io.InputStream} и {@link java.io.OutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {
        Path pathInput = Paths.get(sourceFileName);
        Path pathOut = Paths.get(destinationFileName);
        boolean isSuccessfulCopy = false;
        try (InputStream inputStream = Files.newInputStream(pathInput);
             OutputStream outputStream = Files.newOutputStream(pathOut)
        ) {
            byte[] bufferByte = new byte[1024];
            int sizeReadBlock;
            do {
                sizeReadBlock = inputStream.read(bufferByte, 0, bufferByte.length);
                if (sizeReadBlock > 0) {
                    outputStream.write(bufferByte, 0, sizeReadBlock);
                }
            } while (sizeReadBlock > 0);
            outputStream.flush();
            isSuccessfulCopy = true;

        } catch (IOException eio) {
            System.out.println("Проблемы с вводом/выводом");
            eio.printStackTrace();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return isSuccessfulCopy;
    }


}
