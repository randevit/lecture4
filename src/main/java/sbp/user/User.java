package sbp.user;

public class User {
	private int id; //уникальный идентификатор
	private String role; // Роли: обычный пользователь, администратор и т.д
	private String nickname;
	private String name;
	private String surname;
	private String email;
	private String phone;

	public User(int id, String role, String nickname) {
		this.id = id;
		this.role = role;
		this.nickname = nickname;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

	@Override
	public boolean equals(Object another) {
		return this.id == ((User) another).id;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", role='" + role + '\'' +
				", nickname='" + nickname + '\'' +
				", name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", email='" + email + '\'' +
				", phone='" + phone + '\'' +
				'}';
	}

	public long getId() {
		return id;
	}

	public String getRole() {
		return role;
	}

	public String getNickname() {
		return nickname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
