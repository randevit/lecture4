package sbp.app;

import sbp.db.PersonDAO;
import sbp.person.Person;

public class DbApplications {
    public static void main(String[] args) {
        PersonDAO dao = new PersonDAO("jdbc:sqlite:/home/randy/vfast/java/dbs/test.db");
        Person person1 = new Person("Vasya","Tyumen",22);
        Person person2 = new Person("Kolya","Moskow",45);
        Person person3 = new Person("Vova","Omsk",65);

//        System.out.println(dao.addPerson(person1));
//        System.out.println(dao.addPerson(person2));
//        System.out.println(dao.addPerson(person3));

        System.out.println(dao.readAllPersons());
        System.out.println(dao.deletePersonById(1));

    }
}
