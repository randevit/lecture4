package sbp.exceptions;

public class WorkWithExceptions
{
    /**
     * В данном методе необходимо вызвать методы throwCheckedException и throwUncheckedException.
     * Все исключения должны быть обработаны
     * Необходимо вывести описание exception и stacktrace в консоль
     * Впойманное исключение необходимо упаковать в кастомное исключение и пробросить выше
     * Перед завершением работы метода обязательно необходимо вывести в консоль сообщение "Finish"
     */
    public void exceptionProcessing() throws Exception, RuntimeException {
        try {
            throwCheckedException();
        } catch (Exception ex) {
            System.out.println("Failed in .throwCheckedException() of WorkWithExceptions: " + ex.toString());
            ex.printStackTrace();
            throw new Exception("Checked exception: Failed in .throwCheckedException() of WorkWithExceptions ("+ ex.toString()+")");
        } finally {
            System.out.println("Finish....");
        }

        try {
            throwUncheckedException();
        } catch (RuntimeException ex) {
            System.out.println("Failed in .throwUncheckedException() of WorkWithExceptions: " + ex.toString());
            ex.printStackTrace();
            throw new RuntimeException("Unchecked exception: Failed in .throwUncheckedException() of WorkWithExceptions ("+ ex.toString()+")");
        } finally {
            System.out.println("Finish....");
        }

    }

    /**
     * (* - необязательно) Доп. задание.
     * Выписать в javadoc (здесь) - все варианты оптимизации и устранения недочётов метода
     * Перед finally желателен заградительный перехват самого общего исключения Exception
     * @throws IllegalStateException
     * @throws Exception
     * @throws RuntimeException
     */
    public void hardExceptionProcessing() throws IllegalStateException, Exception, RuntimeException
    {
        System.out.println("Start");
        try
        {
            System.out.println("Step 1");
            throw new IllegalArgumentException();
        }
        catch (IllegalArgumentException e)
        {
            System.out.println("Catch IllegalArgumentException");
            throw new RuntimeException("Step 2");
        }
        catch (RuntimeException e)
        {
            System.out.println("Catch RuntimeException");
            throw new RuntimeException("Step 3");
        }
        finally
        {
            System.out.println("Step finally");
            throw new RuntimeException("From finally");
        }
    }

    private void throwCheckedException() throws Exception
    {
        throw new Exception("Checked exception");
    }

    private void throwUncheckedException()
    {
        throw new RuntimeException("Unchecked exception");
    }
}
