package sbp.io;

import java.io.*;
import java.util.Objects;

public class MyIOExample
{
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName)
    {
        File file = new File(fileName);
        if (file.exists()) {
            boolean isFile = file.isFile();
            System.out.println(file.getAbsolutePath());
            System.out.println(file.getParent());
            if (isFile) {
                System.out.println(file.length());
                System.out.println(file.lastModified());
            }
            return true;
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        File fileInput = new File(sourceFileName);
        File fileOutput = new File(destinationFileName);
        boolean isSuccessfulCopy = false;
        try (FileInputStream inputStream = new FileInputStream(new File(sourceFileName));
             FileOutputStream outputStream = new FileOutputStream(new File(destinationFileName))
            ) {
                byte[] bufferByte = new byte[1024];
                int sizeReadBlock;
                do {
                    sizeReadBlock = inputStream.read(bufferByte, 0, bufferByte.length);
                    if (sizeReadBlock > 0) {
                        outputStream.write(bufferByte, 0, sizeReadBlock);
                    }
                } while (sizeReadBlock > 0);
                outputStream.flush();
                isSuccessfulCopy = true;

            } catch (IOException eio) {
                System.out.println("Проблемы с вводом/выводом");
                eio.printStackTrace();

            } catch (Exception ex) {
                ex.printStackTrace();
        }


        return isSuccessfulCopy;
    }



    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        File fileInput = new File(sourceFileName);
        File fileOutput = new File(destinationFileName);
        boolean isSuccessfulCopy = false;
        try (BufferedInputStream bufInputStream = new BufferedInputStream(new FileInputStream(new File(sourceFileName)));
             BufferedOutputStream bufOutputStream = new BufferedOutputStream(new FileOutputStream(new File(destinationFileName)))
            ) {
                byte[] bufferByte = new byte[1024];
                int sizeReadBlock;
                do {
                    sizeReadBlock = bufInputStream.read(bufferByte, 0, bufferByte.length);
                    if (sizeReadBlock > 0) {
                        bufOutputStream.write(bufferByte, 0, sizeReadBlock);
                    }
                } while (sizeReadBlock > 0);
                bufOutputStream.flush();
                isSuccessfulCopy = true;

            } catch (IOException eio) {
                System.out.println("Проблемы с вводом/выводом");
                eio.printStackTrace();

            } catch (Exception ex) {
                ex.printStackTrace();
        }

        return isSuccessfulCopy;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {
        File fileInput = new File(sourceFileName);
        File fileOutput = new File(destinationFileName);
        boolean isSuccessfulCopy = false;
        try (FileReader fileReader = new FileReader(new File(sourceFileName));
             FileWriter fileWriter = new FileWriter(new File(destinationFileName))
            ) {
                char[] bufferChar = new char[1024];
                int sizeReadBlock;
                do {
                    sizeReadBlock = fileReader.read(bufferChar, 0, bufferChar.length);
                    if (sizeReadBlock > 0) {
                        fileWriter.write(bufferChar, 0, sizeReadBlock);
                    }
                } while (sizeReadBlock > 0);
                fileWriter.flush();
                isSuccessfulCopy = true;

            } catch (IOException eio) {
                System.out.println("Проблемы с вводом/выводом");
                eio.printStackTrace();

            } catch (Exception ex) {
                ex.printStackTrace();
        }

        return isSuccessfulCopy;
    }
}
